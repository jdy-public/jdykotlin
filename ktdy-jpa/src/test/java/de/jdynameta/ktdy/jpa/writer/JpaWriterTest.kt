package de.jdynameta.ktdy.jpa.writer

import de.jdynameta.ktdy.base.dto.TypedReflectionValueObjectWrapper
import de.jdynameta.ktdy.jpa.JpaMetaModelReader
import de.jdynameta.ktdy.jpa.entity.BankAccount
import de.jdynameta.ktdy.jpa.entity.BankAccountRepository
import jakarta.persistence.EntityManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class JpaWriterTest @Autowired constructor(
    val testEntityManager: EntityManager,
    val accountRepository: BankAccountRepository
) {

    @Test
    fun deleteObjectFromDbClassInfo() {


        val account1 = createAccount(1L);

        this.accountRepository.save(account1)
        this.accountRepository.save(createAccount(2L))

        assertEquals(2, this.accountRepository.findAll().count() )

        val metaRepo = JpaMetaModelReader().createJpaClassInfos(testEntityManager.metamodel, "KtDyTestApp")
        val accountInfo = metaRepo.allClassInfos.first { it.entityName ==  BankAccount::class.simpleName}

        JpaWriter().deleleteInDb(TypedReflectionValueObjectWrapper(account1, accountInfo), testEntityManager)
    }

    private fun createAccount(id: Long): BankAccount {
        val place = BankAccount(
            bankCode = "Code$id",
            accountNumber = "account1$id",
            accountHolderName = "account holder 1$id",
            id = id
        )
        return place
    }
}