package de.jdynameta.ktdy.jpa.entity

import org.springframework.data.repository.CrudRepository

interface BankAccountRepository : CrudRepository<BankAccount, Long> {

    fun findByBankCodeOrderByAccountNumberAsc(bankCode: String): List<BankAccount>

}