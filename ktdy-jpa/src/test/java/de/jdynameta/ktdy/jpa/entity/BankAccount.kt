package de.jdynameta.ktdy.jpa.entity

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id

@Entity
class BankAccount(

    @Id @GeneratedValue
    val id: Long? = null,
    var  bankCode:String,
    var  accountNumber:String? = null,
    var  accountHolderName:String? = null,

)
