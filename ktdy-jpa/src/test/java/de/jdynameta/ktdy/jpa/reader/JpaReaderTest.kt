package de.jdynameta.ktdy.jpa.reader

import de.jdynameta.ktdy.jpa.JpaMetaModelReader
import de.jdynameta.ktdy.jpa.entity.BankAccount
import de.jdynameta.ktdy.jpa.entity.BankAccountRepository
import jakarta.persistence.EntityManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager

@DataJpaTest
class JpaReaderTest @Autowired constructor(
    val testEntityManager: EntityManager,
    val accountRepository: BankAccountRepository
) {

    @Test
    fun readAllObjectsFromDbClassInfo() {

        this.accountRepository.save(createAccount(1L))
        this.accountRepository.save(createAccount(2L))

        assertEquals(2, this.accountRepository.findAll().count() )

        val metaRepo = JpaMetaModelReader().createJpaClassInfos(testEntityManager.metamodel, "KtDyTestApp")
        val accountInfo = metaRepo.allClassInfos.first { it.entityName ==  BankAccount::class.simpleName}

        assertEquals(2, JpaReader().readAllObjectsFromDbClassInfo( accountInfo, testEntityManager).size)
    }

    private fun createAccount(id: Long): BankAccount {
        val place = BankAccount(
            bankCode = "Code$id",
            accountNumber = "account1$id",
            accountHolderName = "account holder 1$id",
            id = id
        )
        return place
    }
}