package de.jdynameta.ktdy.jpa


import de.jdynameta.ktdy.base.filter.impl.KtDyClassInfoQuery
import de.jdynameta.ktdy.base.filter.impl.KtDyOperatorEqual
import de.jdynameta.ktdy.base.filter.impl.KtDyOperatorExpression
import de.jdynameta.ktdy.base.info.KtPrimitiveAttributeInfo
import de.jdynameta.ktdy.jpa.entity.BankAccount
import de.jdynameta.ktdy.jpa.entity.BankAccountRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager


@DataJpaTest
class JpaFilterConverterTest @Autowired constructor(
    val testEntityManager: TestEntityManager,
    val accountRepository: BankAccountRepository
)  {

    @Test
    fun `read by filter normal`() {

        val ingBankAccount = BankAccount(bankCode = "ING", accountNumber="123ING456", accountHolderName="JOHN SMITH");
        testEntityManager.persist(ingBankAccount)
        testEntityManager.flush()

        val foundAccounts = accountRepository.findByBankCodeOrderByAccountNumberAsc("ING")
        Assertions.assertEquals(1, foundAccounts.size)
    }

    @Test
    fun `read by filter converter`() {

        val ingBankAccount = BankAccount(bankCode = "ING", accountNumber="123ING456", accountHolderName="JOHN SMITH");
        testEntityManager.persist(ingBankAccount)
        testEntityManager.flush()

        val classRepository = JpaMetaModelReader().createJpaClassInfos(this.testEntityManager.entityManager.metamodel, "TestApp")
        Assertions.assertEquals(1, classRepository.allClassInfos.size)

        val bankAccountMeta = classRepository.allClassInfos
            .filter { classInfo -> classInfo.entityName.equals("BankAccount")}
            .first()

        val attribute = bankAccountMeta.attributeList
            .filter { attr -> attr.attrName.equals("bankCode") }
            .first()

        val value: Comparable<*> = "ING"
        val query = KtDyClassInfoQuery(bankAccountMeta, KtDyOperatorExpression(attribute as KtPrimitiveAttributeInfo,  KtDyOperatorEqual(), value ))
        val jpaFilter = JpaFilterConverter(this.testEntityManager.entityManager).convert(query)
        val result = this.testEntityManager.entityManager.createQuery(jpaFilter).getResultList();
    }
}