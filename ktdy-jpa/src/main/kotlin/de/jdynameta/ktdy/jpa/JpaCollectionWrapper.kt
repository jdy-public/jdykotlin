package de.jdynameta.ktdy.jpa

import jakarta.persistence.Column
import jakarta.persistence.metamodel.Attribute
import jakarta.persistence.metamodel.EntityType
import jakarta.persistence.metamodel.PluralAttribute
import java.lang.reflect.Field

class JpaCollectionWrapper(anAttr: Attribute<*, *>) {

    private var attr: Attribute<*, *> = anAttr
    var field: Field? = null
    var type: PluralAttribute.CollectionType? = null
    var referencedType: EntityType<*>? = null

    init {
        val listAttr: PluralAttribute<*, *, *> = anAttr as PluralAttribute<*, *, *>
        type = listAttr.collectionType
        field = if (anAttr.getJavaMember() is Field) {
            anAttr.getJavaMember() as Field
        } else {
            null
        }
        referencedType = listAttr.elementType as EntityType
    }

    fun getJavaType(): Class<*>? {
        return attr.javaType
    }

    fun <T : Annotation?> getAnnotationInfo(annotation: Class<T>): T? {
        return if (field is Field) {
            field!!.getAnnotation(annotation)
        } else {
            null
        }
    }

    fun isNullable(): Boolean {
        val columnInfo = getAnnotationInfo(Column::class.java)
        return columnInfo?.nullable ?: false
    }

}