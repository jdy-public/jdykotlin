/*******************************************************************************
 * Copyright (c) 2021,2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package de.jdynameta.ktdy.jpa

import de.jdynameta.ktdy.base.info.KtAttributeInfo
import de.jdynameta.ktdy.base.info.KtClassRepository
import de.jdynameta.ktdy.base.info.impl.KtDyAssociationInfo
import de.jdynameta.ktdy.base.info.impl.*
import de.jdynameta.ktdy.base.info.primitive.KtDbDomainValue
import de.jdynameta.ktdy.base.info.primitive.impl.KtDyTimestampType
import de.jdynameta.ktdy.base.info.primitive.KtStringTypeHint
import de.jdynameta.ktdy.base.info.primitive.KtTimestampResolution
import de.jdynameta.ktdy.base.info.primitive.impl.*
import jakarta.persistence.*
import jakarta.persistence.metamodel.*
import jakarta.persistence.metamodel.Attribute.PersistentAttributeType.*
import jakarta.validation.constraints.DecimalMax
import jakarta.validation.constraints.DecimalMin
import jakarta.validation.constraints.Email
import java.math.BigDecimal
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*

class JpaMetaModelReader {

    companion object {
        val DECIMAL_MIN_VALUE: BigDecimal = BigDecimal("-9999999999999.999999")
        val DECIMAL_MAX_VALUE: BigDecimal = BigDecimal("9999999999999.999999")
    }


    fun createJpaClassInfos(metaModel: Metamodel, appName: String): KtClassRepository {

        val classes: MutableCollection<KtDyClassInfo>  = this.createJpaClassInfos(metaModel.entities, metaModel.embeddables)
        val repo =  KtDyClassRepository(appName)
        classes.forEach {
            repo.addClassInfo(it)
        }
        return repo
    }

    private fun createJpaClassInfos(allEntityInfos: Set<EntityType<*>?>, allEmbeddableInfos: Set<EmbeddableType<*>?>): MutableCollection<KtDyClassInfo> {

        val className2InfoMap = mutableMapOf<String, KtDyClassInfo>()
        val allManagedEntities: MutableList<ManagedType<*>> = mutableListOf()
        allManagedEntities.addAll(allEmbeddableInfos.filterNotNull().map { it })
        allManagedEntities.addAll(allEntityInfos.filterNotNull().map { it })

        // build base classes
        allManagedEntities.forEach { jpaEntity ->
            val entity = createJpaClassInfo(jpaEntity)
            className2InfoMap[entity.entityName] = entity
        }

        // add entity attributes
        allManagedEntities.forEach { jpsEntity ->
            className2InfoMap[JpaFieldWrapper.getEntityName(jpsEntity)]?.let { classInfo ->
                buildAttrForClassInfo(classInfo = classInfo, jpaEntity = jpsEntity, className2InfoMap)
            }
        }

        // add associations to entities
        allEntityInfos.forEach { jpaEntity ->
            jpaEntity?.let {
                className2InfoMap[it.name]?.let { classInfo ->
                    buildAssociationsForClassInfo(classInfo = classInfo, jpaEntity = jpaEntity, className2InfoMap)
                }
            }
        }

        return className2InfoMap.values
    }

    private fun createJpaClassInfo(jpaEntity: ManagedType<*>): KtDyClassInfo {

        return KtDyClassInfo(
            entityName = JpaFieldWrapper.getEntityName(jpaEntity),
            nameSpace = jpaEntity.javaType.name.replace('.', '_'),
            embeddable = jpaEntity is EmbeddableType,
            superClass = null
        )
    }

    private fun buildAttrForClassInfo(classInfo: KtDyClassInfo, jpaEntity: ManagedType<*>, className2InfoMap: MutableMap<String, KtDyClassInfo>) {


        jpaEntity.attributes.forEach { attr ->
            if (!attr.isCollection) {
                if (attr.persistentAttributeType === Attribute.PersistentAttributeType.BASIC) {

                    createPrimitiveField(attr)?.let {
                        classInfo.attributeList.add(it)
                    }
                } else if (attr.persistentAttributeType === ONE_TO_ONE || attr.persistentAttributeType === MANY_TO_ONE) {

                    createObjectReference(attr, className2InfoMap)?.let {
                        classInfo.attributeList.add(it)
                    }
                } else if (attr.persistentAttributeType === EMBEDDED) {

                    createObjectReference(attr, className2InfoMap)?.let {
                        classInfo.attributeList.add(it)
                    }
                }
            }
        }
    }


    private fun buildAssociationsForClassInfo(classInfo: KtDyClassInfo, jpaEntity: EntityType<*>, className2InfoMap: MutableMap<String, KtDyClassInfo>) {

        jpaEntity.attributes.forEach {
            if (it.isCollection) {
                if (it.persistentAttributeType === Attribute.PersistentAttributeType.ONE_TO_MANY) {
                    val wrapper = JpaCollectionWrapper(it)
                    val metaDetailClass = className2InfoMap[wrapper.referencedType?.name]
                    val mapping = wrapper.getAnnotationInfo(OneToMany::class.java)
                    val joinColumn = wrapper.getAnnotationInfo(JoinColumn::class.java)
                    val metaAssocName = it.name
                    val metaAssoc = metaDetailClass?.let { it1 -> KtDyAssociationInfo(assocName = metaAssocName, detailClass = it1) }
                    metaAssoc?.let { it1 -> classInfo.associationList.add(it1) }
                }
            }
        }
    }

    private fun createObjectReference(
        curAttr: Attribute<*, *>,
        className2InfoMap: MutableMap<String, KtDyClassInfo>
    ): KtDyObjectReferenceInfo? {

        val wrapper = JpaFieldWrapper(curAttr)
        val isKey = (curAttr as SingularAttribute).isId
        val isNotNull = !curAttr.isOptional || !wrapper.isNullable()
        val isGenerated = wrapper.getGeneratedInfo() != null
        val refTypeName = wrapper.getRefTypeName()
        val referenceType = className2InfoMap[refTypeName]
        val metaAttr = referenceType?.let {
            KtDyObjectReferenceInfo(referencedClass = it, attrName = curAttr.getName(), isKey = isKey, notNull = isNotNull, embedded = wrapper.embedded)
        }

        return metaAttr

    }

    private fun createPrimitiveField(curAttr: Attribute<*, *>): KtAttributeInfo? {
        val wrapper = JpaFieldWrapper(curAttr)
        val metaType = getPrimitiveType(wrapper)
        return if (metaType != null) {

            val isKey = (curAttr as SingularAttribute).isId
            val isNotNull = !curAttr.isOptional || !wrapper.isNullable()
            val isGenerated = wrapper.getGeneratedInfo() != null
            val metaAttr = KtDyPrimitiveAttributeInfo(primitiveType = metaType, attrName = curAttr.name, isKey = isKey, notNull = isNotNull)
            metaAttr.generated = isGenerated
            metaAttr
        } else {
            null
        }
    }

    private fun getPrimitiveType(wrapper: JpaFieldWrapper): KtDyPrimitiveType? {
        val aTypeClass: Class<*>? = wrapper.getJavaType()
        return if (aTypeClass == null) {
            null
        } else if (aTypeClass.isAssignableFrom(UUID::class.java)) {
            KtDyUuidType()
        } else if (aTypeClass.isAssignableFrom(Int::class.javaObjectType)
            || aTypeClass.isAssignableFrom(Int::class.java) || Int::class.javaPrimitiveType!!.isAssignableFrom(aTypeClass)) {
            KtDyLongType(Int.MIN_VALUE.toLong(), Int.MAX_VALUE.toLong())
        } else if ( aTypeClass.isAssignableFrom(Long::class.javaObjectType) || aTypeClass.isAssignableFrom(Long::class.java)
                    || Long::class.javaPrimitiveType!!.isAssignableFrom(aTypeClass)) {
            KtDyLongType(Long.MIN_VALUE, Long.MAX_VALUE)
        } else if ( aTypeClass.isAssignableFrom(Short::class.javaObjectType) || aTypeClass.isAssignableFrom(Short::class.java)
            || Short::class.javaPrimitiveType!!.isAssignableFrom(aTypeClass)) {
           Long::class.java
            KtDyLongType(Short.MIN_VALUE.toLong(), Short.MAX_VALUE.toLong())
        } else if ( aTypeClass.isAssignableFrom(Byte::class.javaObjectType) ||
            aTypeClass.isAssignableFrom(Byte::class.java) || ByteArray::class.java.isAssignableFrom(aTypeClass)) {
            KtDyBlobType()
        } else if (aTypeClass.isAssignableFrom(String::class.javaObjectType) || aTypeClass.isAssignableFrom(String::class.java)) {
            val column = wrapper.getAnnotationInfo(Column::class.java)
            val length = column?.length ?: 40
            val email = wrapper.getAnnotationInfo(Email::class.java)
            if (email == null) KtDyStringType(length.toLong()) else KtDyStringType(length.toLong(), KtStringTypeHint.EMAIL)
        } else if (aTypeClass.isAssignableFrom(Date::class.java) || aTypeClass.isAssignableFrom(Timestamp::class.java)
            || aTypeClass.isAssignableFrom(Instant::class.java)
        ) {
            val temporal = wrapper.getAnnotationInfo(jakarta.persistence.Temporal::class.java)
            val temporalType: TemporalType = temporal?.value ?: TemporalType.TIMESTAMP
            val resolution: KtTimestampResolution;
            resolution = if (temporalType === TemporalType.TIMESTAMP) {
                KtTimestampResolution.DATE_TIME
            } else if (temporalType === TemporalType.DATE) {
                KtTimestampResolution.DATE
            } else {
                KtTimestampResolution.TIME
            }
            KtDyTimestampType(resolution)
        } else if (aTypeClass.isAssignableFrom(LocalDate::class.java)) {
            KtDyTimestampType(KtTimestampResolution.DATE)
        } else if (aTypeClass.isAssignableFrom(LocalTime::class.java)) {
            KtDyTimestampType(KtTimestampResolution.TIME)
        } else if (aTypeClass.isAssignableFrom(LocalDateTime::class.java)) {
            KtDyTimestampType(KtTimestampResolution.DATE_TIME)
        } else if (aTypeClass.isAssignableFrom(Boolean::class.java) || Boolean::class.javaPrimitiveType!!.isAssignableFrom(aTypeClass)) {
            KtDyBooleanType()
        } else if (aTypeClass.isAssignableFrom(Double::class.java) || aTypeClass.isAssignableFrom(Float::class.java)
            || Double::class.javaPrimitiveType!!.isAssignableFrom(aTypeClass) || Float::class.javaPrimitiveType!!.isAssignableFrom(aTypeClass)
        ) {
            KtDyFloatType()
        } else if (aTypeClass.isAssignableFrom(BigDecimal::class.java)) {
            val column = wrapper.getAnnotationInfo(Column::class.java)
            var scale = 0
            var precision = 0
            if (column != null) {
                scale = column.scale
                precision = column.precision
            }
            val decMin: DecimalMin? = wrapper.getAnnotationInfo(DecimalMin::class.java)
            val decMax: DecimalMax? = wrapper.getAnnotationInfo(DecimalMax::class.java)
            val minValue = if (decMin != null) BigDecimal(decMin.value) else DECIMAL_MIN_VALUE
            val maxValue = if (decMax != null) BigDecimal(decMax.value) else DECIMAL_MAX_VALUE
            KtDyDecimalType(minValue, maxValue, scale)
        } else {
            if (aTypeClass.isEnum) {
                val column = wrapper.getAnnotationInfo(Column::class.java)
                val length = column?.length ?: 40
                val domainValues: MutableList<KtDbDomainValue> = ArrayList<KtDbDomainValue>()
                for (jpaField in aTypeClass.declaredFields) {
                    if (jpaField.isEnumConstant) {
                        domainValues.add(KtDyDbDomainValue(jpaField.name, jpaField.name))
                    }
                }
                return KtDyStringType(length = length.toLong(), typeHint = KtStringTypeHint.ENUM
                    , domValues = domainValues, domValueType =wrapper.type.javaType)
            }
            null
        }
    }

}


