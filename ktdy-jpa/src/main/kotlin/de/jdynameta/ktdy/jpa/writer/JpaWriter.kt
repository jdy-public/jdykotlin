package de.jdynameta.ktdy.jpa.writer

import de.jdynameta.ktdy.base.dto.TypedValueObject
import de.jdynameta.ktdy.base.exception.InvalidClassInfoException
import de.jdynameta.ktdy.base.filter.KtFilterExpression
import de.jdynameta.ktdy.base.filter.impl.KtDyAndExpression
import de.jdynameta.ktdy.base.filter.impl.KtDyClassInfoQuery
import de.jdynameta.ktdy.base.filter.impl.KtDyOperatorEqual
import de.jdynameta.ktdy.base.filter.impl.KtDyOperatorExpression
import de.jdynameta.ktdy.base.info.KtObjectReferenceInfo
import de.jdynameta.ktdy.base.info.KtPrimitiveAttributeInfo
import de.jdynameta.ktdy.jpa.JpaFilterConverter
import jakarta.persistence.EntityManager


class JpaWriter {

     fun deleleteInDb(objToDelete: TypedValueObject, entityManager: EntityManager) {
        val idExprList: MutableList<KtFilterExpression> = ArrayList<KtFilterExpression>()

        objToDelete.getClassInfo().attributeList.forEach {attrInfo ->

            val compareValue = objToDelete.getValue(attrInfo)
                ?: throw InvalidClassInfoException("Value is null for attribute " + attrInfo.attrName);
            if (attrInfo is KtPrimitiveAttributeInfo) {
                if (attrInfo.isKey) {
                    val opExpr = KtDyOperatorExpression(
                        attribute = attrInfo,
                        operator = KtDyOperatorEqual.EQUAL_INSTANCE,
                        compareValue = compareValue)

                    idExprList.add(opExpr)
                }
            } else if (attrInfo is KtObjectReferenceInfo) {
                if (attrInfo.isKey) {
                    throw InvalidClassInfoException("Unknown Attribute Type")
                }
            } else {
                throw InvalidClassInfoException("Unknown Attribute Type")
            }

        }

       val query =
           KtDyClassInfoQuery(resultInfo = objToDelete.getClassInfo()
               , filterExpression = KtDyAndExpression(idExprList)
           )
        val criteriaQuery = JpaFilterConverter(entityManager).convert(query)
        val jpaObjToDelete: Any = entityManager.createQuery(criteriaQuery).getSingleResult()
        if (jpaObjToDelete != null) {
            entityManager.remove(jpaObjToDelete)
        }
    }
}