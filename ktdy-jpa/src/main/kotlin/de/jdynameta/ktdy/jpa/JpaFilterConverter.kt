package de.jdynameta.ktdy.jpa

import de.jdynameta.ktdy.base.filter.*
import de.jdynameta.ktdy.base.filter.operator.*
import jakarta.persistence.EntityManager
import jakarta.persistence.criteria.*
import jakarta.persistence.metamodel.EntityType
import java.util.*

class JpaFilterConverter(
    private val entityManager: EntityManager,
) {

    fun convert(jdyQuery: KtClassInfoQuery): CriteriaQuery<out Any>?  {

        val entityTypeHolder = getJpaEntityTypeForClassName(jdyQuery.resultInfo.entityName)
        val entityType = entityTypeHolder.get()
        val criteriaBuilder = entityManager.criteriaBuilder
        val criteriaQuery: CriteriaQuery<Any>? = criteriaBuilder.createQuery(entityType.javaType) as CriteriaQuery<Any>?

        val result: CriteriaQuery<out Any>? = criteriaQuery?.let {
            val entityRoot = criteriaQuery.from(entityType.javaType)
            val rootExpression = jdyQuery.filterExpression
            val rootPredicate =
                rootExpression.visit(JpaVisitor(criteriaBuilder, entityRoot))

            if (entityRoot != null) {
               return criteriaQuery.select(entityRoot).where(rootPredicate);
            } else {
                return null
            }
        }
        return result
    }


    private fun getJpaEntityTypeForClassName(className: String): Optional<EntityType<out Any>> {
        return entityManager.metamodel.entities
            .stream()
            .filter { entity -> entity.name.equals(className) }
            .findFirst()
    }


    private class JpaVisitor(val criteriaBuilder: CriteriaBuilder, entityRoot: From<out Any, out Any>) :
        ExpressionVisitor<Predicate?> {
        val entityRoot: From<*, *>

        init {
            this.entityRoot = entityRoot
        }


        override fun visitAndExpression(anAndExpr: KtAndExpression): Predicate? {

            val subPredicates: MutableList<Predicate> = ArrayList()
            anAndExpr.expressions.forEach {
                val predicate = it.visit(this)
                if (predicate != null) {
                    subPredicates.add(predicate)
                }
            }
            return criteriaBuilder.and(*subPredicates.toTypedArray())
        }

        override fun visitOrExpression(anOrExpression: KtOrExpression): Predicate {

            val subPredicates: MutableList<Predicate> = ArrayList()
            anOrExpression.expressions.forEach {
                val predicate = it.visit(this)
                if (predicate != null) {
                    subPredicates.add(predicate)
                }
            }
            return criteriaBuilder.or(*subPredicates.toTypedArray())
        }

        override fun visitOperatorExpression(anOpExpr: KtOperatorExpression): Predicate? {
            val visitor =
                JpaOperatorVisitor<Comparable<Any>>(
                    entityRoot, criteriaBuilder, anOpExpr
                )
            return anOpExpr.operator.visitOperatorHandler(visitor)
        }
    }


    private class JpaOperatorVisitor<Y : Comparable<Any>> constructor(
        entityRoot: From<*, *>,
        criteriaBuilder: CriteriaBuilder,
        aOpExpr: KtOperatorExpression,
    ) :
        OperatorVisitor<Predicate> {
        val entityRoot: From<*, *>
        private val criteriaBuilder: CriteriaBuilder
        private val compareValue: Comparable<Any>
        private val attrPath: Path<Y>

        init {

            this.entityRoot = entityRoot
            this.criteriaBuilder = criteriaBuilder
            val attrName: String = aOpExpr.attribute.attrName
            compareValue = aOpExpr.compareValue as Comparable<Any>
            attrPath = entityRoot.get(attrName)
        }

        override fun visitOperatorEqual(aOperator: KtOperatorEqual): Predicate {
            return criteriaBuilder.equal(attrPath, compareValue)
        }

        override fun visitOperatorNotEqual(aOperator: KtOperatorNotEqual): Predicate {
            return criteriaBuilder.notEqual(attrPath, compareValue)
        }

        override fun visitOperatorGreater(aOperator: KtOperatorGreater): Predicate? {

              return if (aOperator.isAlsoEqual) {
                criteriaBuilder.greaterThanOrEqualTo(attrPath, compareValue)
            } else {
                criteriaBuilder.greaterThan(attrPath, compareValue)
            }
        }

        override fun visitOperatorLess(aOperator: KtOperatorLess): Predicate? {
            return if (aOperator.isAlsoEqual) {
                criteriaBuilder.lessThanOrEqualTo(attrPath, compareValue)
            } else {
                criteriaBuilder.lessThan(attrPath, compareValue)
            }
        }

    }

}
