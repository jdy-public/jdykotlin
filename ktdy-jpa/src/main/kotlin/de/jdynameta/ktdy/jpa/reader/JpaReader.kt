package de.jdynameta.ktdy.jpa.reader

import de.jdynameta.ktdy.base.info.KtClassInfo
import de.jdynameta.ktdy.jpa.exception.KtJdyJpaException
import jakarta.persistence.EntityManager
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.metamodel.EntityType

class JpaReader {


    fun readAllObjectsFromDbClassInfo(classInfo: KtClassInfo, entityManager: EntityManager): List<Any> {

        val enitityType = this.getJpaEntityTypeForClassName(classInfo, entityManager)
        if(enitityType != null) {
            return readAllObjectsFromDbForEntity(enitityType, entityManager)
        } else {
            throw KtJdyJpaException("Invalid class info")
        }
    }


    private fun readAllObjectsFromDbForEntity(entityType: EntityType<*>, entityManager: EntityManager): List<Any> {

        val criteriaBuilder: CriteriaBuilder = entityManager.getCriteriaBuilder()
        val query: CriteriaQuery<*> = criteriaBuilder.createQuery(entityType.javaType)
        query.from(entityType.javaType)

        return entityManager.createQuery(query).resultList
    }

    private fun getJpaEntityTypeForClassName(classInfo: KtClassInfo, entityManager: EntityManager): EntityType<*>? {

        return entityManager.metamodel.entities
            .first { entity -> entity.name.equals(classInfo.entityName) }
    }
}