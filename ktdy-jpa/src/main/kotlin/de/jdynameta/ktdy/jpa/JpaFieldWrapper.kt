package de.jdynameta.ktdy.jpa

import jakarta.persistence.Column
import jakarta.persistence.GeneratedValue
import jakarta.persistence.metamodel.*
import java.lang.reflect.Field
import java.lang.reflect.Member

class JpaFieldWrapper(anAttr: Attribute<*, *>) {

    companion object {
        fun getEntityName(jpaEntity: Type<*>): String {

            return when (jpaEntity) {
                is EntityType -> jpaEntity.name
                is EmbeddableType -> jpaEntity.getJavaType().simpleName
                else -> ""
            }
        }
    }

    private var attr: Attribute<*, *> = anAttr
    var field: Member? = null
    val type: Type<*>
    val embedded: Boolean

    init {
        val singAttr: SingularAttribute<*, *> = anAttr as SingularAttribute
        this.type = singAttr.type
        this.field = if (anAttr.getJavaMember() is Field) {
            anAttr.getJavaMember()
        } else {
            null
        }
        this.embedded = attr.persistentAttributeType === Attribute.PersistentAttributeType.EMBEDDED
    }

    fun <T : Annotation?> getAnnotationInfo(annotation: Class<T>): T? {
        return if (field is Field) {
            (field as Field?)!!.getAnnotation(annotation)
        } else {
            null
        }
    }

    fun getJavaType(): Class<*>? {
        return attr.javaType
    }

    fun isNullable(): Boolean {
        val columnInfo = getAnnotationInfo(Column::class.java)
        return columnInfo?.nullable ?: true
    }

    fun getGeneratedInfo(): Any? {
        return getAnnotationInfo(GeneratedValue::class.java)
    }

    fun getRefTypeName(): String {

        return getEntityName(type)

    }
}