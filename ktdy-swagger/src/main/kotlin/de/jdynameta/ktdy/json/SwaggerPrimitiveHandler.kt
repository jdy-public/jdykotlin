package de.jdynameta.ktdy.json

import de.jdynameta.ktdy.base.info.primitive.*
import de.jdynameta.ktdy.base.info.primitive.impl.KtDyUuidType
import io.swagger.v3.oas.models.SpecVersion
import io.swagger.v3.oas.models.media.Schema
import java.math.BigDecimal
import java.time.Instant
import java.util.*

class SwaggerPrimitiveHandler : PrimitiveTypeVisitor {

    private var classSchema: Schema<String>? = null
    private var curPrimitivName: String? = null

    fun setCurPrimitivName(classSchema: Schema<String>, curPrimitivName: String) {
        this.classSchema = classSchema
        this.curPrimitivName = curPrimitivName
    }

    override fun handleValue(aValue: Long?, aType: KtLongType) {

        val property = Schema<String>(SpecVersion.V31)
            .type("integer")
            .format("int64")

        property.minimum(aType.minValue?.
            let { BigDecimal.valueOf(it) })
        classSchema!!.addProperty(this.curPrimitivName, property);
    }

    override fun handleValue(aValue: Boolean?, aType: KtBooleanType) {
        classSchema!!.addProperty(this.curPrimitivName, Schema<String>(SpecVersion.V31).type("boolean"))
    }

    override fun handleValue(aValue: Double?, aType: KtFloatType) {
        classSchema!!.addProperty(this.curPrimitivName, Schema<String>(SpecVersion.V31).type("number").format("double"))
    }

    override fun handleValue(aValue: Instant?, aType: KtTimestampType) {
        classSchema!!.addProperty(this.curPrimitivName, Schema<String>(SpecVersion.V31).type("string").format("date-time"))
    }

    override fun handleValue(aValue: BigDecimal?, aType: KtDecimalType) {
        classSchema!!.addProperty(this.curPrimitivName, Schema<String>(SpecVersion.V31).type("number"))
    }

    override fun handleValue(aValue: String?, aType: KtTextType) {
        classSchema!!.addProperty(this.curPrimitivName, Schema<String>(SpecVersion.V31).type("string"))
    }

    override fun handleValue(aValue: String?, aType: KtStringType) {

        val property =  Schema<String>(SpecVersion.V31)
            .type("string")
            .maxLength(aType.length.toInt())
        classSchema!!.addProperty(this.curPrimitivName, property)
        if(aType.domValues != null && aType.domValues!!.isNotEmpty()) {
            val values = aType.domValues!!.map {
                it.domValue
            }
            property.enum = values
        }
    }

    override fun handleValue(aValue: ByteArray?, aType: KtBlobType) {
       classSchema!!.addProperty(this.curPrimitivName, Schema<String>(SpecVersion.V31).type("string").format("byte"))
    }

    override fun handleValue(aValue: UUID?, aType: KtDyUuidType) {
        classSchema!!.addProperty(this.curPrimitivName, Schema<String>(SpecVersion.V31).type("string").format("uuid"))
    }
}