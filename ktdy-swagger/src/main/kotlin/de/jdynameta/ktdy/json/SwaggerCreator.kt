package de.jdynameta.ktdy.json

import de.jdynameta.ktdy.base.exception.InvalidClassInfoException
import de.jdynameta.ktdy.base.info.KtClassInfo
import de.jdynameta.ktdy.base.info.KtObjectReferenceInfo
import de.jdynameta.ktdy.base.info.KtPrimitiveAttributeInfo
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.SpecVersion
import io.swagger.v3.oas.models.media.Content
import io.swagger.v3.oas.models.media.MediaType
import io.swagger.v3.oas.models.media.Schema
import io.swagger.v3.oas.models.parameters.RequestBody
import io.swagger.v3.oas.models.responses.ApiResponse
import io.swagger.v3.oas.models.responses.ApiResponses

class SwaggerCreator {

    fun createGetPath(classInfo: KtClassInfo, tag:String): Operation? {

        val refProperty = Schema<String>(SpecVersion.V31).type("array").`$ref`("#/components/schemas/" + entityName(classInfo))

        val operation = Operation()
            .tags(listOf(tag))
            .summary("Generated Summary " + entityName(classInfo))
            .description("Generated description " + entityName(classInfo))
            .operationId("Get" + entityName(classInfo))
            .responses(
                ApiResponses().addApiResponse("200x", ApiResponse()
                .description("Generated Response " + entityName(classInfo))
                .content(Content().addMediaType("application/json", MediaType().schema(refProperty)))))


        return operation;
    }

    fun createPostPath(classInfo: KtClassInfo, tag:String): Operation? {

        val refProperty = Schema<String>(SpecVersion.V31).type("array").`$ref`("#/components/schemas/" + entityName(classInfo))

        val operation = Operation()
            .tags(listOf(tag))
            .summary("Create a new " + entityName(classInfo))
            .description("Create a new Object of " + entityName(classInfo))
            .operationId("Create" + entityName(classInfo))
            .requestBody(RequestBody().content(Content().addMediaType("application/json", MediaType().schema(refProperty))))
            .responses(
                ApiResponses().addApiResponse("200x", ApiResponse()
                    .description("New created " + entityName(classInfo))
                    .content(Content().addMediaType("application/json", MediaType().schema(refProperty)))))


        return operation;
    }

    fun createSchema(classInfo: KtClassInfo, asProxy: Boolean): Schema<String> {

        val primitiveHandler = SwaggerPrimitiveHandler()

        val classSchema = Schema<String>(SpecVersion.V31)

        classSchema.type("object")
            .name(entityName(classInfo))
            .description(entityName(classInfo) + " Description")
            .title(entityName(classInfo) + " Title")

        for (attrInfo in classInfo.attributeList) {
            if (!asProxy || attrInfo.isKey) {
                if (attrInfo is KtObjectReferenceInfo) {
                    val refProperty = Schema<String>(SpecVersion.V31)
                    refProperty.`$ref` = "#/components/schemas/" + entityName(attrInfo.referencedClass)
                    classSchema.addProperty(attrInfo.attrName, refProperty)

                } else if (attrInfo is KtPrimitiveAttributeInfo) {
                    primitiveHandler.setCurPrimitivName(classSchema, attrInfo.attrName)
                    attrInfo.primitiveType.handlePrimitiveKey(primitiveHandler, null)

                } else {
                    throw InvalidClassInfoException("Unknown Attribute Type")
                }
            }
        }

        for (assocInfo in classInfo.associationList) {
            if (!asProxy) {
                val refProperty = Schema<String>(SpecVersion.V31)
                refProperty.`$ref` = "#/components/schemas/" + entityName(assocInfo.detailClass)
                val assocProperty = Schema<String>(SpecVersion.V31)
                    .type("array")
                    .description("List of " + entityName(assocInfo.detailClass))
                    .items(refProperty)
                classSchema.addProperty(assocInfo.assocName, assocProperty)
            }
        }

        return classSchema
    }

    fun entityName(classInfo: KtClassInfo): String {
        return classInfo.entityName
    }
}