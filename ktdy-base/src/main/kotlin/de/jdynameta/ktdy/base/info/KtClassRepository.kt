package de.jdynameta.ktdy.base.info

interface KtClassRepository {
    val repoName: String
    val allClassInfos: List<KtClassInfo>
    fun infoWithName(name2Search: String) : KtClassInfo?
}