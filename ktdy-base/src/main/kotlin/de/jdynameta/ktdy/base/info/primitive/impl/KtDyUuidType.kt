package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtUuidType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor
import java.time.Instant
import java.util.*

class KtDyUuidType : KtDyPrimitiveType, KtUuidType {

    override fun getTypeName(): String {
        return "UUID"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else value as UUID, this)
    }
}