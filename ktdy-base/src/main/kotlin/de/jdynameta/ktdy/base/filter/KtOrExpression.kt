package de.jdynameta.ktdy.base.filter

interface KtOrExpression : KtFilterExpression {
    val expressions: List<KtFilterExpression>
}