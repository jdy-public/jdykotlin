package de.jdynameta.ktdy.base.info.impl

import de.jdynameta.ktdy.base.info.KtClassInfo
import de.jdynameta.ktdy.base.info.KtClassRepository

class KtDyClassRepository (
    override val repoName: String
) : KtClassRepository {

    override val allClassInfos: MutableList<KtClassInfo> = mutableListOf()

    fun addClassInfo(aInfo: KtClassInfo) {

        this.allClassInfos.add(aInfo)
    }

    override fun infoWithName(name2Search: String) : KtClassInfo? {

        return this.allClassInfos
            .firstOrNull { it.entityName ==  name2Search}
    }


    fun addClassInfo(entityName: String): KtDyClassInfo {

        var newClassInfo = KtDyClassInfo(entityName=entityName, nameSpace=repoName, superClass = null)
        this.allClassInfos.add(newClassInfo)
        return newClassInfo
    }

    companion object {

        fun addAssociation(
            assocName: String,
            masterClass: KtDyClassInfo,
            detailClass: KtDyClassInfo,
        ) {
            val newAssoc = KtDyAssociationInfo(assocName, detailClass)
            masterClass.associationList.add(newAssoc)
        }
    }
}