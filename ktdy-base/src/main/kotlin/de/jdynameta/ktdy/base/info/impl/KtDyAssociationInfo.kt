package de.jdynameta.ktdy.base.info.impl

import de.jdynameta.ktdy.base.info.KtAssociationInfo

class KtDyAssociationInfo(
    override val assocName: String,
    override val detailClass: KtDyClassInfo
) : KtAssociationInfo