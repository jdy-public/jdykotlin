package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.KtClassInfoQuery
import de.jdynameta.ktdy.base.filter.KtFilterExpression
import de.jdynameta.ktdy.base.filter.value.KtAttributePath
import de.jdynameta.ktdy.base.info.KtClassInfo

class KtDyClassInfoQuery (
    override val resultInfo: KtClassInfo,
    override val filterExpression: KtFilterExpression,
    override val selectAttributes: List<KtAttributePath> = emptyList()
) : KtClassInfoQuery {

}