package de.jdynameta.ktdy.base.info

import de.jdynameta.ktdy.base.info.primitive.KtPrimitiveType

interface KtPrimitiveAttributeInfo : KtAttributeInfo {
    val primitiveType: KtPrimitiveType
    override val attrName: String
    override val isKey: Boolean
    override val generated: Boolean
    override val notNull: Boolean
}