package de.jdynameta.ktdy.base.filter.operator

import de.jdynameta.ktdy.base.filter.KtPrimitiveOperator

interface KtOperatorGreater: KtPrimitiveOperator {

    val isAlsoEqual: Boolean
}