package de.jdynameta.ktdy.base.filter

import de.jdynameta.ktdy.base.info.KtPrimitiveAttributeInfo

interface KtOperatorExpression : KtFilterExpression {

    val attribute: KtPrimitiveAttributeInfo
    val operator: KtPrimitiveOperator
    val compareValue: Any
}