package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtFloatType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor
import java.math.BigDecimal

class KtDyFloatType : KtDyPrimitiveType, KtFloatType {

    override fun getTypeName(): String {
        return "Float"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else (value as Number).toDouble(), this)
    }
}