package de.jdynameta.ktdy.base.info.primitive

enum class KtStringTypeHint {
    TELEPHONE,
    URL,
    ENUM,
    EMAIL,
    UUID;

    val dbValue: String
        get() = name
    val representation: String
        get() = name
}