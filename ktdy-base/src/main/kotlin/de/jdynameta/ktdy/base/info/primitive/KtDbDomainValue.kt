package de.jdynameta.ktdy.base.info.primitive

interface KtDbDomainValue {
    val domValue: String
    val representation: String
}