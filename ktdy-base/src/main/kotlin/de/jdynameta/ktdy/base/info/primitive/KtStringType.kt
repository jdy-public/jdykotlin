package de.jdynameta.ktdy.base.info.primitive

interface KtStringType : KtPrimitiveType {
    val length: Long
    val typeHint: KtStringTypeHint?
    val domValues: List<KtDbDomainValue>?
    val domValueType: Class<out Any>?
}