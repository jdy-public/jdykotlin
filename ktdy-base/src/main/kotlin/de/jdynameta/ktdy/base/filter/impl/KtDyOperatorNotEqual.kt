package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.operator.KtOperatorEqual
import de.jdynameta.ktdy.base.filter.operator.KtOperatorNotEqual
import de.jdynameta.ktdy.base.filter.operator.OperatorVisitor

class KtDyOperatorNotEqual (

) : KtOperatorNotEqual  {

    override fun <OpertorType> visitOperatorHandler(aVisitor: OperatorVisitor<OpertorType>): OpertorType? {
        return aVisitor.visitOperatorNotEqual(this)
    }

    companion object {
        val NOT_EQUAL_INSTANCE = KtDyOperatorNotEqual()
    }
}