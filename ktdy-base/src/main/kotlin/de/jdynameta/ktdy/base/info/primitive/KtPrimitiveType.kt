package de.jdynameta.ktdy.base.info.primitive

interface KtPrimitiveType {
    fun getTypeName(): String

    fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?)
}