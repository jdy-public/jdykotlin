package de.jdynameta.ktdy.base.filter.operator

interface OperatorVisitor<TOpertorType> {

    fun visitOperatorEqual(aOperator: KtOperatorEqual): TOpertorType?

    fun visitOperatorNotEqual(aOperator: KtOperatorNotEqual): TOpertorType?

    fun visitOperatorGreater(aOperator: KtOperatorGreater): TOpertorType?

    fun visitOperatorLess(aOperator: KtOperatorLess): TOpertorType?
}