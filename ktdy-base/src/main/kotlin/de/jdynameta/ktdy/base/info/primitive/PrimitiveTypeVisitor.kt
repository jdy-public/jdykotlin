package de.jdynameta.ktdy.base.info.primitive

import de.jdynameta.ktdy.base.info.primitive.impl.KtDyUuidType
import java.math.BigDecimal
import java.time.Instant
import java.util.*

interface PrimitiveTypeVisitor {


    fun handleValue(aValue: Long?, aType: KtLongType)

    
    fun handleValue(aValue: Boolean?, aType: KtBooleanType)

    
    fun handleValue(aValue: Double?, aType: KtFloatType)

    
    fun handleValue(aValue: Instant?, aType: KtTimestampType)

    
    fun handleValue(aValue: BigDecimal?, aType: KtDecimalType)

    
    fun handleValue(aValue: String?, aType: KtTextType)

    
    fun handleValue(aValue: String?, aType: KtStringType)

    
    fun handleValue(aValue: ByteArray?, aType: KtBlobType)

    fun handleValue(aValue: UUID?, aType: KtDyUuidType)

}