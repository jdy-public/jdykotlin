package de.jdynameta.ktdy.base.info

interface KtObjectReferenceInfo : KtAttributeInfo {
    val referencedClass: KtClassInfo
    val embedded: Boolean
    override val attrName: String
    override val isKey: Boolean
    override val generated: Boolean
    override val notNull: Boolean
}