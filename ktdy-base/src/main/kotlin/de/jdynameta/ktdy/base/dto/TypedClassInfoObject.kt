package de.jdynameta.ktdy.base.dto

import de.jdynameta.ktdy.base.info.KtClassInfo

interface TypedClassInfoObject {

    fun getClassInfo(): KtClassInfo
}