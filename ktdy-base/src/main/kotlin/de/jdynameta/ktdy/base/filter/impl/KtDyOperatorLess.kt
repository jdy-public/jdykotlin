package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.operator.KtOperatorEqual
import de.jdynameta.ktdy.base.filter.operator.KtOperatorGreater
import de.jdynameta.ktdy.base.filter.operator.KtOperatorLess
import de.jdynameta.ktdy.base.filter.operator.OperatorVisitor

class KtDyOperatorLess (

    override val isAlsoEqual: Boolean
) : KtOperatorLess  {

    override fun <OpertorType> visitOperatorHandler(aVisitor: OperatorVisitor<OpertorType>): OpertorType? {
        return aVisitor.visitOperatorLess(this)
    }
}