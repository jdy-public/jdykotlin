package de.jdynameta.ktdy.base.info.impl

import de.jdynameta.ktdy.base.info.KtPrimitiveAttributeInfo
import de.jdynameta.ktdy.base.info.primitive.KtPrimitiveType

class KtDyPrimitiveAttributeInfo(
    override val primitiveType: KtPrimitiveType,
    override val attrName: String,
    override var isKey: Boolean = false,
    override var generated: Boolean = false,
    override var notNull: Boolean = false

) : KtPrimitiveAttributeInfo