package de.jdynameta.ktdy.base.info.impl

import de.jdynameta.ktdy.base.info.KtClassInfo
import de.jdynameta.ktdy.base.info.KtObjectReferenceInfo

class KtDyObjectReferenceInfo(
    override val referencedClass: KtClassInfo,
    override val embedded: Boolean = false,
    override val attrName: String,
    override val isKey: Boolean = false,
    override val generated: Boolean = false,
    override val notNull: Boolean = false,

) : KtObjectReferenceInfo