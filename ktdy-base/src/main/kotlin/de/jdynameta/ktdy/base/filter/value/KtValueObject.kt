package de.jdynameta.ktdy.base.filter.value

import de.jdynameta.ktdy.base.info.KtAssociationInfo
import de.jdynameta.ktdy.base.info.KtAttributeInfo

interface KtValueObject {

    fun getValue(aInfo: KtAttributeInfo): Any?
    fun getValue(aInfo: KtAssociationInfo): List<Any?>
}