package de.jdynameta.ktdy.base.filter.value

import de.jdynameta.ktdy.base.info.KtAttributeInfo
import de.jdynameta.ktdy.base.info.KtObjectReferenceInfo

interface KtAttributePath {

    val LastInfo: KtAttributeInfo
    val Path: List<KtObjectReferenceInfo>

}