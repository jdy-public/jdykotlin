package de.jdynameta.ktdy.base.info.primitive

interface KtTextType : KtPrimitiveType {
    val length: Long
    val typeHint: KtTextMimeType?
}