package de.jdynameta.ktdy.base.filter

interface KtAndExpression : KtFilterExpression {
    val expressions: List<KtFilterExpression>
}