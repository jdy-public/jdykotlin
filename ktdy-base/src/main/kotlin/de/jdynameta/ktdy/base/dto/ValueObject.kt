package de.jdynameta.ktdy.base.dto

import de.jdynameta.ktdy.base.info.KtAssociationInfo
import de.jdynameta.ktdy.base.info.KtAttributeInfo

interface ValueObject {

    /**
     * Get the value of the object for the given Attribute info
     *
     * @param aInfo
     * @return
     */
    fun getValue(aInfo: KtAttributeInfo): Any?

    /**
     * Check, if the ValueObject has an Valid Value for the given AttributeInfo
     * Either the value has not been set in the Model or the Object does not
     * belong to the ClassInfo of the Attribute
     *
     */
    fun hasValueFor(aInfo: KtAttributeInfo): Boolean

    /**
     * Get the value of the object for the given AssociationInfo info
     *
     * @param aInfo
     * @return
     */
    fun getValue(aInfo: KtAssociationInfo): List<ValueObject>?

    /**
     * Check, if the ValueObject has an Valid Value for the given
     * AssociationInfo Either the value has not been set in the Model or the
     * Object does not belong to the ClassInfo of the AssociationInfo
     *
     */
    fun hasValueFor(aInfo: KtAssociationInfo): Boolean
}