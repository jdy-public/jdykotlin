package de.jdynameta.ktdy.base.info.primitive

interface KtLongType : KtPrimitiveType {
    val minValue: Long?
    val maxValue: Long?
    val domValues: List<KtDbDomainValue>?
}