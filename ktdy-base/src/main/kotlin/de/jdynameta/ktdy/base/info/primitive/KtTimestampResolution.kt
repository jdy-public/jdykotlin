package de.jdynameta.ktdy.base.info.primitive

enum class KtTimestampResolution {
    TIME,
    DATE,
    DATE_TIME;

    val dbValue: String
        get() = name
    val representation: String
        get() = name
}