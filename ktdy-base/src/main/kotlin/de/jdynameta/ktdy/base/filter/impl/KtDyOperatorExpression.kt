package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.*
import de.jdynameta.ktdy.base.info.KtPrimitiveAttributeInfo

class KtDyOperatorExpression  (
    override val attribute: KtPrimitiveAttributeInfo,
    override val operator: KtPrimitiveOperator,
    override val compareValue: Any
) : KtOperatorExpression  {

    override fun <TResult> visit(visitor: ExpressionVisitor<TResult>): TResult? {
        return visitor.visitOperatorExpression(this)
    }
}