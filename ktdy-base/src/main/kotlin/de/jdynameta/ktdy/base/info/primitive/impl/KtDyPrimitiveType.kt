package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtPrimitiveType

interface KtDyPrimitiveType : KtPrimitiveType {

    override fun getTypeName(): String
}