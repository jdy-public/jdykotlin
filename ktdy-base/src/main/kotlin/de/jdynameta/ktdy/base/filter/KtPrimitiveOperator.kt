package de.jdynameta.ktdy.base.filter

import de.jdynameta.ktdy.base.filter.operator.OperatorVisitor

interface KtPrimitiveOperator {

    fun <OpertorType> visitOperatorHandler(aVisitor: OperatorVisitor<OpertorType>): OpertorType?
}