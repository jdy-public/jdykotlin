package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.operator.KtOperatorEqual
import de.jdynameta.ktdy.base.filter.operator.OperatorVisitor

class KtDyOperatorEqual (

) : KtOperatorEqual {

    override fun <OpertorType> visitOperatorHandler(aVisitor: OperatorVisitor<OpertorType>): OpertorType? {
        return aVisitor.visitOperatorEqual(this)
    }

    companion object {
        val EQUAL_INSTANCE = KtDyOperatorEqual()
    }
}