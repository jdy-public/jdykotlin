package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.ExpressionVisitor
import de.jdynameta.ktdy.base.filter.KtAndExpression
import de.jdynameta.ktdy.base.filter.KtFilterExpression

class KtDyAndExpression (
    override val expressions: MutableList<KtFilterExpression>
) : KtAndExpression {
    override fun <TResult> visit(visitor: ExpressionVisitor<TResult>): TResult? {
        return visitor.visitAndExpression(this)
    }
}