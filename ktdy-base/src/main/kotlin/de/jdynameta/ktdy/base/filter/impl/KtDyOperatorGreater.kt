package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.operator.KtOperatorEqual
import de.jdynameta.ktdy.base.filter.operator.KtOperatorGreater
import de.jdynameta.ktdy.base.filter.operator.OperatorVisitor

class KtDyOperatorGreater (

    override val isAlsoEqual: Boolean


) : KtOperatorGreater {

    override fun <OpertorType> visitOperatorHandler(aVisitor: OperatorVisitor<OpertorType>): OpertorType? {
        return aVisitor.visitOperatorGreater(this)
    }
}