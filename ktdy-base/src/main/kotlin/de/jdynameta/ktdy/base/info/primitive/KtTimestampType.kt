package de.jdynameta.ktdy.base.info.primitive

interface KtTimestampType : KtPrimitiveType {
    val resolution: KtTimestampResolution
}