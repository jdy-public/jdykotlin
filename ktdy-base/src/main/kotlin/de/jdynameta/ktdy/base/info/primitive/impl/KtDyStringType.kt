package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtDbDomainValue
import de.jdynameta.ktdy.base.info.primitive.KtStringTypeHint
import de.jdynameta.ktdy.base.info.primitive.KtStringType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor

class KtDyStringType(
    override val length: Long,
    override val typeHint: KtStringTypeHint? = null,
    override val domValues: List<KtDbDomainValue>? = null,
    override val domValueType: Class<out Any>? = null
) : KtDyPrimitiveType, KtStringType {

    override fun getTypeName(): String {
        return "String"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else value.toString(), this)
    }
}