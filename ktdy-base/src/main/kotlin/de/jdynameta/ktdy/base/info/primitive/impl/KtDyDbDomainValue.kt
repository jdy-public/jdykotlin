package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtDbDomainValue


class KtDyDbDomainValue(
    override val domValue: String,
    override val representation: String,
)  : KtDbDomainValue