package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtDbDomainValue
import de.jdynameta.ktdy.base.info.primitive.KtDecimalType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor
import java.math.BigDecimal

class KtDyDecimalType(
    override val minValue: BigDecimal?,
    override val maxValue: BigDecimal?,
    override val scale: Int,
    override val domValues: List<KtDbDomainValue>? = null
) : KtDyPrimitiveType, KtDecimalType {
    override fun getTypeName(): String {
        return "BigDecimal"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else (value as BigDecimal), this)
    }
}