package de.jdynameta.ktdy.base.filter.operator

import de.jdynameta.ktdy.base.filter.KtPrimitiveOperator

interface KtOperatorLess: KtPrimitiveOperator {

    val isAlsoEqual: Boolean
}