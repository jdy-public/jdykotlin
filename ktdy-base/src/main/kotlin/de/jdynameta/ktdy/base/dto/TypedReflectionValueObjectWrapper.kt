package de.jdynameta.ktdy.base.dto

import de.jdynameta.ktdy.base.info.*
import mu.KotlinLogging
import java.lang.reflect.Field
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.util.*

class TypedReflectionValueObjectWrapper(
    private var wrappedObject: Any,
    private var classInfo: KtClassInfo
) : TypedValueObject {

    private val logger = KotlinLogging.logger {}


    override fun getClassInfo(): KtClassInfo {
        return classInfo
    }

    fun getWrappedObject(): Any? {
        return wrappedObject
    }

    override fun getValue(aInfo: KtAttributeInfo): Any? {
        return try {
            var result: Any? = null
            if (wrappedObject != null) {
                if (aInfo is KtPrimitiveAttributeInfo) {
                    val methodName = stringWithFirstLetterLowercase(aInfo.attrName)
                    val me: Class<out Any> = wrappedObject.javaClass
                    val getter = Arrays.stream(me.getMethods())
                        .filter { method: Method -> method.name == methodName }
                        .findFirst()
                    if (getter.isPresent) {
                        result = getter.get().invoke(wrappedObject, null as Array<Any?>?)
                    } else {
                        val nameField: Field? = getFieldsRecursive(me, aInfo.attrName)
                        if (nameField != null) {
                            nameField.setAccessible(true)
                            val type = nameField.type
                            result = nameField[wrappedObject]
                        } else {
                            logger.error("Could not find getter : " + aInfo.attrName)
                        }
                    }
                } else {
                    val me: Class<out Any> = wrappedObject.javaClass
                    val methodName = "get"+ stringWithFirstLetterUppercase(aInfo.attrName)
                    val getter = Arrays.stream(me.getMethods())
                        .filter { method: Method -> method.name == methodName }
                        .findFirst()
                    result = getter.get().invoke(wrappedObject)
                    if (result !is ValueObject && result != null) {
                        result = TypedReflectionValueObjectWrapper(
                            result,
                            (aInfo as KtObjectReferenceInfo).referencedClass
                        )
                    }
                }
            }
            result
        } catch (ex: SecurityException) {
            logger.error("Error reading Field", ex)
            null
        } catch (ex: IllegalAccessException) {
            logger.error("Error reading Field", ex)
            null
        } catch (ex: IllegalArgumentException) {
            logger.error("Error reading Field", ex)
            null
        } catch (ex: NoSuchMethodException) {
            logger.error("Error reading Field", ex)
            null
        } catch (ex: InvocationTargetException) {
            logger.error("Error reading Field", ex)
            null
        }
    }

    override fun getValue(aInfo: KtAssociationInfo): List<ValueObject>? {
        try {
            val resultColl: MutableList<ValueObject> = mutableListOf()
            val me: Class<out Any> = wrappedObject.javaClass

            val methodName1 = "get" + stringWithFirstLetterUppercase(aInfo.assocName)
            val getter = Arrays.stream(me.methods)
                .filter { method: Method -> method.name == methodName1 }
                .findFirst()

            if (getter.isPresent) {
                val assocColl = getter.get().invoke(wrappedObject) as Collection<Any>

                if (assocColl != null) {
                    for (`object` in assocColl) {
                        resultColl.add(TypedReflectionValueObjectWrapper(`object`, aInfo.detailClass))
                    }
                }
            } else {
                logger.error("Could not find getter for assoc : " + aInfo.assocName)
            }

            return resultColl
        } catch (e: SecurityException) {
            e.printStackTrace()
            return null
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
            return null
        } catch (e: java.lang.IllegalArgumentException) {
            e.printStackTrace()
            return null
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
            return null
        }
    }

    override fun hasValueFor(aInfo: KtAttributeInfo): Boolean {

        // TODO Auto-generated method stub
        return true
    }

    override fun hasValueFor(aInfo: KtAssociationInfo): Boolean {
        // TODO Auto-generated method stub
        return true
    }

    fun stringWithFirstLetterUppercase(textToAppend: String): String {
        return "" + textToAppend[0].uppercaseChar() + textToAppend.substring(1)
    }

    fun stringWithFirstLetterLowercase(textToAppend: String): String {
        return "" + textToAppend[0].lowercaseChar() + textToAppend.substring(1)
    }

    fun getFieldsRecursive(clazz: Class<*>, fieldName: String): Field? {
        val getterField = Arrays.stream(clazz.getDeclaredFields())
            .filter { field: Field -> field.name == fieldName }
            .findFirst()
        return if (getterField.isPresent) {
            getterField.get()
        } else if (clazz.superclass != null) {
            getFieldsRecursive(clazz.superclass, fieldName)
        } else {
            null
        }
    }

}