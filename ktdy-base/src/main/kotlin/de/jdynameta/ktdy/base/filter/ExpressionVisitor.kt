package de.jdynameta.ktdy.base.filter

interface ExpressionVisitor<TResult> {

    fun visitAndExpression(anAndExpr: KtAndExpression): TResult?
    fun visitOperatorExpression(anOpExpr: KtOperatorExpression): TResult?
    fun visitOrExpression(anOrExpression: KtOrExpression): TResult?
}