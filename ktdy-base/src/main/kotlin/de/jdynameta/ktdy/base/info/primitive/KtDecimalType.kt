package de.jdynameta.ktdy.base.info.primitive

import java.math.BigDecimal

interface KtDecimalType : KtPrimitiveType {
    val minValue: BigDecimal?
    val maxValue: BigDecimal?
    val scale: Int
    val domValues: List<KtDbDomainValue>?
}