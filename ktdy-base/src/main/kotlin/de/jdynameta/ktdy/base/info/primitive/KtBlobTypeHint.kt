package de.jdynameta.ktdy.base.info.primitive

enum class KtBlobTypeHint {
    ANY,
    IMAGE,
    GIF,
    JPEG,
    PNG;

    val dbValue: String
        get() = name
    val representation: String
        get() = name
}