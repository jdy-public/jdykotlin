package de.jdynameta.ktdy.base.info

import de.jdynameta.ktdy.base.info.impl.KtDyClassInfo

interface KtClassInfo {
    val entityName: String
    val nameSpace: String
    val attributeList: List<KtAttributeInfo>
    val associationList: List<KtAssociationInfo>
    val allSubclasses: List<KtDyClassInfo>
    val superClass: KtClassInfo?
    val embeddable: Boolean
}

