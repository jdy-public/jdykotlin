package de.jdynameta.ktdy.base.info

import de.jdynameta.ktdy.base.info.impl.KtDyClassInfo

interface KtAssociationInfo{
    val assocName: String
    val detailClass: KtDyClassInfo
}
