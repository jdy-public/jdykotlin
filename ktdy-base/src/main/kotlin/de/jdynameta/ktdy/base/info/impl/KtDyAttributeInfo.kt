package de.jdynameta.ktdy.base.info.impl

import de.jdynameta.ktdy.base.info.KtAttributeInfo

abstract class KtDyAttributeInfo(
    override val attrName: String,
    override val isKey: Boolean = false,
    override var generated: Boolean = false,
    override val notNull: Boolean = false
): KtAttributeInfo