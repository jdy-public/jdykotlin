package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtTimestampType
import de.jdynameta.ktdy.base.info.primitive.KtTimestampResolution
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor
import java.time.Instant
import java.util.*

class KtDyTimestampType(
    override val resolution: KtTimestampResolution,
) : KtDyPrimitiveType, KtTimestampType {

    override fun getTypeName(): String {
        return "DateTime"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else value as Instant, this)
    }
}