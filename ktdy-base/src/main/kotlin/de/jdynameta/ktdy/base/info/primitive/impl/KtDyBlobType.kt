package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtBlobType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor

class KtDyBlobType : KtDyPrimitiveType, KtBlobType {

    override fun getTypeName(): String {
        return "Blob"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else (value as ByteArray), this)
    }
}