package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtDbDomainValue
import de.jdynameta.ktdy.base.info.primitive.KtLongType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor

class KtDyLongType(
    override val minValue: Long?,
    override val maxValue: Long?,
    override val domValues: List<KtDbDomainValue>? = null
) : KtDyPrimitiveType, KtLongType {

    override fun getTypeName(): String {
        return "Long"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else (value as Number).toLong(), this)
    }
}