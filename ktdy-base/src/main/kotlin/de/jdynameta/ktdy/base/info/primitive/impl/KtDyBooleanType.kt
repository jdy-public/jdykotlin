package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtBooleanType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor
import java.math.BigDecimal

class KtDyBooleanType : KtDyPrimitiveType, KtBooleanType {

    override fun getTypeName(): String {
        return "Boolean"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else (value as Boolean), this)
    }
}