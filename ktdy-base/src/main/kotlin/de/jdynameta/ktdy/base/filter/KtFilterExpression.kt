package de.jdynameta.ktdy.base.filter

interface KtFilterExpression {

    fun <TResult> visit(visitor: ExpressionVisitor<TResult>): TResult?
}