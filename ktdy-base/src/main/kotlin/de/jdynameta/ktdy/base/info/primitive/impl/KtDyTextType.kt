package de.jdynameta.ktdy.base.info.primitive.impl

import de.jdynameta.ktdy.base.info.primitive.KtTextMimeType
import de.jdynameta.ktdy.base.info.primitive.KtTextType
import de.jdynameta.ktdy.base.info.primitive.PrimitiveTypeVisitor

class KtDyTextType(
    override val length: Long,
    override val typeHint: KtTextMimeType? = null,
) : KtDyPrimitiveType, KtTextType {

    override fun getTypeName(): String {
        return "String"
    }

    override fun handlePrimitiveKey(aHandler: PrimitiveTypeVisitor, value: Any?) {
        aHandler.handleValue(if ((value == null)) null else value.toString(), this)
    }
}