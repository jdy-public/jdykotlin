package de.jdynameta.ktdy.base.info

import de.jdynameta.ktdy.base.info.impl.KtDyClassInfo

interface KtAttributeInfo {
    val attrName: String
    val isKey: Boolean
    val generated: Boolean
    val notNull: Boolean
}
