package de.jdynameta.ktdy.base.info.primitive

enum class KtTextMimeType(
    override val domValue: String,
    override val representation: String,
    ): KtDbDomainValue {
    XML("text/xml", "text/xml"),
    HTML("text/html", "text/xml"),
    PLAIN("text/plain", "text/xml");
}