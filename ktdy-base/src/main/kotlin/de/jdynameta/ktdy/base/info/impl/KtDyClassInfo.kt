package de.jdynameta.ktdy.base.info.impl

import de.jdynameta.ktdy.base.info.KtAssociationInfo
import de.jdynameta.ktdy.base.info.KtAttributeInfo
import de.jdynameta.ktdy.base.info.KtClassInfo
import de.jdynameta.ktdy.base.info.primitive.KtDbDomainValue
import de.jdynameta.ktdy.base.info.primitive.KtTimestampResolution
import de.jdynameta.ktdy.base.info.primitive.impl.*
import java.math.BigDecimal
import java.util.*

class KtDyClassInfo(
    override val entityName: String,
    override val nameSpace: String,
    override val attributeList: MutableList<KtAttributeInfo> = mutableListOf(),
    override val associationList: MutableList<KtAssociationInfo> = mutableListOf(),
    override val allSubclasses: MutableList<KtDyClassInfo> = mutableListOf(),
    override val superClass: KtClassInfo?,
    override val embeddable: Boolean = false

) : KtClassInfo {

    fun addLongAttr(attrName: String, aMinValue: Long, aMaxValue: Long, vararg allDomainValues: KtDbDomainValue)
            : KtDyPrimitiveAttributeInfo {

        val domainValueList: List<KtDbDomainValue> = listOf(*allDomainValues)
        val primitiveType = KtDyLongType(aMinValue, aMaxValue, domainValueList)

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }

    fun addDecimalAttr(attrName: String, aMinValue: BigDecimal, aMaxValue: BigDecimal, aScale: Int
                       , vararg allDomainValues: KtDbDomainValue)
            : KtDyPrimitiveAttributeInfo {

        val domainValueList: List<KtDbDomainValue> = listOf(*allDomainValues)
        val primitiveType = KtDyDecimalType(aMinValue, aMaxValue, aScale, domainValueList)

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }

    fun addBooleanAttr(attrName: String)  : KtDyPrimitiveAttributeInfo {

        val primitiveType = KtDyBooleanType()

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }

    fun addFloatAttr(attrName: String)  : KtDyPrimitiveAttributeInfo {

        val primitiveType = KtDyFloatType()

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }


    fun addBlobAttr(attrName: String)  : KtDyPrimitiveAttributeInfo {

        val primitiveType = KtDyBlobType()

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }

    fun addTextAttr(attrName: String,  aLength: Long )  : KtDyPrimitiveAttributeInfo {

        val primitiveType = KtDyTextType(length=aLength)

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }

    fun addStringAttr(attrName: String,  aLength: Long, vararg allDomainValues: KtDbDomainValue )  : KtDyPrimitiveAttributeInfo {

        val domainValueList: List<KtDbDomainValue> = listOf(*allDomainValues)
        val primitiveType = KtDyStringType(length=aLength, domValues=domainValueList)

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }

    fun addTimestampAttr(attrName: String, resolution: KtTimestampResolution)  : KtDyPrimitiveAttributeInfo {

        val primitiveType = KtDyTimestampType(resolution)

        val newAttr = KtDyPrimitiveAttributeInfo(primitiveType = primitiveType, attrName = attrName)
        this.attributeList.add(newAttr)
        return newAttr
    }

    fun addReference( attrName: String, aReferencedClass: KtClassInfo): KtDyObjectReferenceInfo {
        val newRef =
            KtDyObjectReferenceInfo(referencedClass=aReferencedClass, attrName= attrName)
        this.attributeList.add(newRef)
        return newRef
    }

}

