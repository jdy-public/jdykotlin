package de.jdynameta.ktdy.base.dto

import de.jdynameta.ktdy.base.info.KtAttributeInfo
import de.jdynameta.ktdy.base.info.KtClassInfo

interface TypedValueObject : ValueObject {
    fun getClassInfo(): KtClassInfo
}