package de.jdynameta.ktdy.base.filter

import de.jdynameta.ktdy.base.filter.value.KtAttributePath
import de.jdynameta.ktdy.base.info.KtClassInfo

interface KtClassInfoQuery {

    val resultInfo: KtClassInfo
    val filterExpression: KtFilterExpression
    val selectAttributes: List<KtAttributePath>
}

