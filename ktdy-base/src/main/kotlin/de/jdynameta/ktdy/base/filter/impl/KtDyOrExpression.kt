package de.jdynameta.ktdy.base.filter.impl

import de.jdynameta.ktdy.base.filter.ExpressionVisitor
import de.jdynameta.ktdy.base.filter.KtFilterExpression
import de.jdynameta.ktdy.base.filter.KtOrExpression

class KtDyOrExpression (
    override val expressions: MutableList<KtFilterExpression>
) : KtOrExpression {
    override fun <TResult> visit(visitor: ExpressionVisitor<TResult>): TResult? {
        return visitor.visitOrExpression(this)
    }
}
