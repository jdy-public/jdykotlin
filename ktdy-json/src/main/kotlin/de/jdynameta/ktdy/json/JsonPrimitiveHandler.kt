package de.jdynameta.ktdy.json

import com.fasterxml.jackson.databind.node.ObjectNode
import de.jdynameta.ktdy.base.info.primitive.*
import de.jdynameta.ktdy.base.info.primitive.impl.KtDyUuidType
import java.math.BigDecimal
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

class JsonPrimitiveHandler : PrimitiveTypeVisitor {

    private var parentJsonObj: ObjectNode? = null
    private var curPrimitivName: String? = null

    fun setCurPrimitivName(aParentJsonObject: ObjectNode?, curPrimitivName: String?) {
        this.parentJsonObj = aParentJsonObject
        this.curPrimitivName = curPrimitivName
    }

    override fun handleValue(aValue: Long?, aType: KtLongType) {

        parentJsonObj!!.put(curPrimitivName, aValue)
    }

    override fun handleValue(aValue: Boolean?, aType: KtBooleanType) {
        parentJsonObj!!.put(curPrimitivName, aValue)
    }

    override fun handleValue(aValue: Double?, aType: KtFloatType) {
        parentJsonObj!!.put(curPrimitivName, aValue)
    }

    override fun handleValue(aValue: Instant?, aType: KtTimestampType) {
        if (aValue != null) {
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            val zdtString = formatter.withZone(ZoneId.of("UTC")).format(aValue)

            parentJsonObj!!.put(curPrimitivName, zdtString)
        } else {
            parentJsonObj!!.putNull(curPrimitivName)
        }
    }

    override fun handleValue(aValue: BigDecimal?, aType: KtDecimalType) {
        parentJsonObj!!.put(curPrimitivName, aValue)
    }

    override fun handleValue(aValue: String?, aType: KtTextType) {
        parentJsonObj!!.put(curPrimitivName, aValue)
    }

    override fun handleValue(aValue: String?, aType: KtStringType) {
        parentJsonObj!!.put(curPrimitivName, aValue)
    }

    override fun handleValue(aValue: ByteArray?, aType: KtBlobType) {
        if (aValue != null) {
            val encoded = Base64.getEncoder().encodeToString(aValue)
            parentJsonObj!!.put(curPrimitivName, encoded)
        } else {
            parentJsonObj!!.putNull(curPrimitivName)
        }
    }

    override fun handleValue(aValue: UUID?, aType: KtDyUuidType) {
        if (aValue != null) {
            parentJsonObj!!.put(curPrimitivName, aValue.toString())
        } else {
            parentJsonObj!!.putNull(curPrimitivName)
        }
    }
}