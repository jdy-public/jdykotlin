package de.jdynameta.ktdy.json

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import de.jdynameta.ktdy.base.dto.TypedValueObject
import de.jdynameta.ktdy.base.exception.InvalidClassInfoException
import de.jdynameta.ktdy.base.info.KtClassInfo
import de.jdynameta.ktdy.base.info.KtObjectReferenceInfo
import de.jdynameta.ktdy.base.info.KtPrimitiveAttributeInfo
import java.io.Writer

class JsonWriter  {

    enum class Operation {
        INSERT,
        UPDATE,
        DELETE,
        READ,
        PROXY
    }

    val CLASS_INTERNAL_NAME_TAG: String = "@classInternalName"
    val NAMESPACE_TAG: String = "@namespace"
    val PERSISTENCE_TAG: String = "@persistence"


    var mapper: ObjectMapper = ObjectMapper()
    var writeStrategy: WriteReferenceStrategy = WriteReferenceStrategy.WriteAllStrategy()
    var primitiveHandler = JsonPrimitiveHandler()
    var flush = true

    fun writeObjectList(out: Writer?, objectList: List<TypedValueObject>, persistenceType: Operation) {
        val jsonObjects = writeModelCollection( objectList, persistenceType)
        val jsonGenerator = mapper.factory.createGenerator(out)
        jsonGenerator.useDefaultPrettyPrinter()

        mapper.configure(SerializationFeature.FLUSH_AFTER_WRITE_VALUE, this.flush)
        mapper.writeValue(jsonGenerator, jsonObjects)
    }

    private fun writeModelCollection(
        aModelColl: List<TypedValueObject>,
        aPersistenceType: Operation
    ): ArrayNode {
        val jsonArray: ArrayNode = mapper.createArrayNode()
        val modelIter: Iterator<TypedValueObject?> = aModelColl.iterator()
        while (modelIter.hasNext()) {
            val curObj: TypedValueObject? = modelIter.next()
            if (curObj != null) {
                val newJsonObj: ObjectNode = writeObjectToJson(curObj, aPersistenceType)
                jsonArray.add(newJsonObj)
            }
        }

        return jsonArray
    }

    private fun writeObjectToJson(objToWrite: TypedValueObject, aPersistenceType: Operation): ObjectNode {
        val jsonObject: ObjectNode = createClassInfoNode(objToWrite, aPersistenceType, false)
        return jsonObject
    }

    private fun createClassInfoNode(
        objToWrite: TypedValueObject,
        aPersistenceType: Operation,
        asProxy: Boolean
    ): ObjectNode {
        val jsonObject = mapper.createObjectNode()
        addMetaDataFields(jsonObject, objToWrite.getClassInfo(), if ((asProxy)) Operation.PROXY else aPersistenceType)

        for (attrInfo in objToWrite.getClassInfo().attributeList) {
            if (!asProxy || attrInfo.isKey) {
                if (attrInfo is KtObjectReferenceInfo) {
                    val refObj = objToWrite.getValue(attrInfo) as TypedValueObject?
                    if (refObj != null) {
                        val isProxy = asProxy || writeStrategy.writeAsProxy(
                            objToWrite.getClassInfo(),
                            attrInfo,
                            refObj
                        )
                        val refJsonNode: ObjectNode = createClassInfoNode(refObj, aPersistenceType, isProxy)
                        jsonObject.set(attrInfo.attrName, refJsonNode);
                    } else {
                        jsonObject.putNull(attrInfo.attrName)
                    }
                } else if (attrInfo is KtPrimitiveAttributeInfo) {
                    val primObj: Any? = objToWrite.getValue(attrInfo)
                    primitiveHandler.setCurPrimitivName(jsonObject, attrInfo.attrName)
                    attrInfo.primitiveType.handlePrimitiveKey(primitiveHandler, primObj)
                } else {
                    throw InvalidClassInfoException("Unknown Attribute Type")
                }
            }
        }

        for (assocInfo in objToWrite.getClassInfo().associationList) {
            if (!writeStrategy.writeAsProxy(objToWrite.getClassInfo(), assocInfo) && !asProxy) {
                val aModelColl: List<TypedValueObject> =
                    objToWrite.getValue(assocInfo) as List<out TypedValueObject>

                var jsonArray: ArrayNode = mapper.createArrayNode()
                if (aModelColl != null) {
                    jsonArray = writeModelCollection(aModelColl, aPersistenceType)
                }
                jsonObject.replace(assocInfo.assocName, jsonArray)
            }
        }
        return jsonObject
    }

    private fun addMetaDataFields(jsonObject: ObjectNode, aClassInfo: KtClassInfo, aPersistenceType: Operation?) {
        jsonObject.put(NAMESPACE_TAG, aClassInfo.nameSpace)
        jsonObject.put(CLASS_INTERNAL_NAME_TAG, aClassInfo.entityName)
        jsonObject.put(PERSISTENCE_TAG, if ((aPersistenceType == null)) "" else aPersistenceType.name)
    }


}