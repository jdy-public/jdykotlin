package de.jdynameta.ktdy.json

import de.jdynameta.ktdy.base.dto.TypedValueObject
import de.jdynameta.ktdy.base.info.KtAssociationInfo
import de.jdynameta.ktdy.base.info.KtClassInfo
import de.jdynameta.ktdy.base.info.KtObjectReferenceInfo
import de.jdynameta.ktdy.base.info.impl.KtDyClassInfo

interface WriteReferenceStrategy {

    fun writeAsProxy( classInfo: KtClassInfo, attrInfo: KtObjectReferenceInfo, refObj: TypedValueObject): Boolean

    fun writeAsProxy(classInfo: KtClassInfo, assocInfo: KtAssociationInfo): Boolean

    class WriteAllStrategy : WriteReferenceStrategy {

        override fun writeAsProxy(classInfo: KtClassInfo, attrInfo: KtObjectReferenceInfo, refObj: TypedValueObject)
        : Boolean {
            return false
        }

        override fun writeAsProxy(classInfo: KtClassInfo, assocInfo: KtAssociationInfo): Boolean {
            return false
        }
    }
}