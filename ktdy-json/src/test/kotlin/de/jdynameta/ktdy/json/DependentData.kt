package de.jdynameta.ktdy.json

data class DependentData (
    val idDependent: String,
    val dependentData: String,
)
