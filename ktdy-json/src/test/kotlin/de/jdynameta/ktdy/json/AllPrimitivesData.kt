package de.jdynameta.ktdy.json

import java.math.BigDecimal
import java.time.Instant

data class AllPrimitivesData(
    val blobData: ByteArray,
    val booleanData: Boolean,
    val integerData: Long,
    val textData: String,
    val dateData: Instant,
    val decimalData: BigDecimal,
    val floatData: Double,
    val longData: Long,
    val dateTimeData: Instant,
    val timeData: Instant,
    val stringData: String,
    )
