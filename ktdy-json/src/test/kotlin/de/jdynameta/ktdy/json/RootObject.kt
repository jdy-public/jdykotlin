package de.jdynameta.ktdy.json

data class RootObject(
    val idRoot : String,
    val primitivData: AllPrimitivesData,
    val dependentObjects: List<DependentData>
)