package de.jdynameta.ktdy.json

import de.jdynameta.ktdy.base.dto.TypedReflectionValueObjectWrapper
import de.jdynameta.ktdy.base.info.impl.KtDyClassInfo
import de.jdynameta.ktdy.base.info.impl.KtDyClassRepository
import de.jdynameta.ktdy.base.info.primitive.KtTimestampResolution
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.StringWriter
import java.math.BigDecimal
import java.time.Instant

class JsonWriterTest {

    @Test
    fun writeObjectList_PrimitiveObject() {

        val repo = KtDyClassRepository("TestJson")
        val allPrimitivesInfo = addPrimitiveDataInfo(repo)

        val dateValue = Instant.parse("2020-08-30T00:00:00.000Z")
        val timeValue = Instant.parse("2020-01-01T18:43:00.000Z")
        val dateTimeValue = Instant.parse("2020-08-30T18:43:00.000Z")
        val dataObject = AllPrimitivesData(
            blobData = "BlobStringValue".toByteArray(),
            booleanData = true,
            integerData = 23423,
            textData = "TextDateValue",
            decimalData = BigDecimal("5533.785"),
            dateData = dateValue,
            floatData = 123.4,
            longData = 23094203952835,
            timeData = timeValue,
            dateTimeData = dateTimeValue,
            stringData = "StringDataValue"
        )

        val writerOut = StringWriter()
        JsonWriter().writeObjectList(
            out = writerOut,
            objectList = listOf(TypedReflectionValueObjectWrapper(dataObject, allPrimitivesInfo)),
            persistenceType = JsonWriter.Operation.READ
        )

        val result = """[ {
            "@namespace" : "TestJson",
            "@classInternalName" : "AllPrimitivesData",
            "@persistence" : "READ",
            "booleanData" : true,
            "blobData" : "QmxvYlN0cmluZ1ZhbHVl",
            "integerData" : 23423,
            "textData" : "TextDateValue",
            "decimalData" : 5533.785,
            "dateData" : "2020-08-30T00:00:00.000+0000",
            "floatData" : 123.4,
            "longData" : 23094203952835,
            "timeData" : "2020-01-01T18:43:00.000+0000",
            "dateTimeData" : "2020-08-30T18:43:00.000+0000",
            "stringData" : "StringDataValue"
        } ]"""
        assertThat(writerOut.toString()).isEqualToIgnoringWhitespace(result)
    }

    @Test
    fun writeObjectList_RootObject() {
        val repo = KtDyClassRepository("TestJson")
        val allPrimitivesInfo = addPrimitiveDataInfo(repo)
        val rootInfo = addRootDataInfo(repo, allPrimitivesInfo)
        val dependentInfo = addDependentDataInfo(repo)
        KtDyClassRepository.addAssociation(RootObject::dependentObjects.name, rootInfo, dependentInfo )

        val dateValue = Instant.parse("2020-08-30T00:00:00.000Z")
        val timeValue = Instant.parse("2020-01-01T18:43:00.000Z")
        val dateTimeValue = Instant.parse("2020-08-30T18:43:00.000Z")
        val dataObject = AllPrimitivesData(
            blobData = "BlobStringValue".toByteArray(),
            booleanData = true,
            integerData = 23423,
            textData = "TextDateValue",
            decimalData = BigDecimal("5533.785"),
            dateData = dateValue,
            floatData = 123.4,
            longData = 23094203952835,
            timeData = timeValue,
            dateTimeData = dateTimeValue,
            stringData = "StringDataValue"
        )
        val dependent1 = DependentData(idDependent="Dependent1", dependentData="data1")
        val rootObject = RootObject(idRoot="RootId", primitivData = dataObject, dependentObjects= listOf(dependent1))

        val writerOut = StringWriter()
        JsonWriter().writeObjectList(
            out = writerOut,
            objectList = listOf(TypedReflectionValueObjectWrapper(rootObject, rootInfo)),
            persistenceType = JsonWriter.Operation.READ
        )

        val result = """[ {
  "@namespace" : "TestJson",
  "@classInternalName" : "RootObject",
  "@persistence" : "READ",
  "idRoot" : "RootId",
  "primitivData" : {
    "@namespace" : "TestJson",
    "@classInternalName" : "AllPrimitivesData",
    "@persistence" : "READ",
    "booleanData" : true,
    "blobData" : "QmxvYlN0cmluZ1ZhbHVl",
    "integerData" : 23423,
    "textData" : "TextDateValue",
    "decimalData" : 5533.785,
    "dateData" : "2020-08-30T00:00:00.000+0000",
    "floatData" : 123.4,
    "longData" : 23094203952835,
    "timeData" : "2020-01-01T18:43:00.000+0000",
    "dateTimeData" : "2020-08-30T18:43:00.000+0000",
    "stringData" : "StringDataValue"
  },
  "dependentObjects" : [ {
    "@namespace" : "TestJson",
    "@classInternalName" : "DependentData",
    "@persistence" : "READ",
    "idDependent" : "Dependent1",
    "dependentData" : "data1"
  } ]
} ]"""
        assertThat(writerOut.toString()).isEqualToIgnoringWhitespace(result)
    }

    private fun addRootDataInfo(repo: KtDyClassRepository, primitiveInfo:KtDyClassInfo): KtDyClassInfo {

        val rootInfo = repo.addClassInfo(RootObject::class.simpleName!!)
        rootInfo.addStringAttr(RootObject::idRoot.name, 100)
        rootInfo.addReference(RootObject::primitivData.name, primitiveInfo)

        return rootInfo;
    }

    private fun addDependentDataInfo(repo: KtDyClassRepository): KtDyClassInfo {

        val dependentInfo = repo.addClassInfo(DependentData::class.simpleName!!)
        dependentInfo.addStringAttr(DependentData::idDependent.name, 100)
        dependentInfo.addStringAttr(DependentData::dependentData.name, 250)

        return dependentInfo;
    }


    private fun addPrimitiveDataInfo(repo: KtDyClassRepository): KtDyClassInfo {

        val allPrimitivesInfo = repo.addClassInfo(AllPrimitivesData::class.simpleName!!)

        allPrimitivesInfo.addBooleanAttr(AllPrimitivesData::booleanData.name)
        allPrimitivesInfo.addBlobAttr(AllPrimitivesData::blobData.name)
        allPrimitivesInfo.addLongAttr(
            AllPrimitivesData::integerData.name,
            Int.MIN_VALUE.toLong(),
            Int.MAX_VALUE.toLong()
        ).isKey = true
        allPrimitivesInfo.addTextAttr(AllPrimitivesData::textData.name, 5000)
        allPrimitivesInfo.addDecimalAttr(
            AllPrimitivesData::decimalData.name, BigDecimal("-999999999999.999"),
            BigDecimal("999999999999.999"), 3
        )
        allPrimitivesInfo.addTimestampAttr(AllPrimitivesData::dateData.name, KtTimestampResolution.DATE)
        allPrimitivesInfo.addFloatAttr(AllPrimitivesData::floatData.name)
        allPrimitivesInfo.addLongAttr(AllPrimitivesData::longData.name, Long.MIN_VALUE, Long.MAX_VALUE)
        allPrimitivesInfo.addTimestampAttr(AllPrimitivesData::timeData.name, KtTimestampResolution.DATE_TIME)
        allPrimitivesInfo.addTimestampAttr(AllPrimitivesData::dateTimeData.name, KtTimestampResolution.TIME)
        allPrimitivesInfo.addStringAttr(AllPrimitivesData::stringData.name, 200)
        return allPrimitivesInfo
    }
}

