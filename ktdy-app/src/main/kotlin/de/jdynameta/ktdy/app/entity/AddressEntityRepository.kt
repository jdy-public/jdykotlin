
package de.jdynameta.ktdy.app.entity

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AddressEntityRepository : CrudRepository<AddressEntity, Long> {

    fun findByName( name: String): AddressEntity;
}