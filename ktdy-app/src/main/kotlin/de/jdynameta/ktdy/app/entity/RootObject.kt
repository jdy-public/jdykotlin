package de.jdynameta.ktdy.app.entity

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "ROOT_OBJECT")
data class RootObject(

    @Id
    @Column(name = "uuid", nullable = false, updatable = false, unique = true, columnDefinition = "uuid")
    val uuid: UUID = UUID.randomUUID(),

    val nameRoot : String,

    @ManyToOne
    val primitivData: AllPrimitivesData,

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    val dependentObjects: List<DependentData>
)