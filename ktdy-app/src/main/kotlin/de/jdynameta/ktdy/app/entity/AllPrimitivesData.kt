package de.jdynameta.ktdy.app.entity

import io.swagger.v3.oas.annotations.media.Schema
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.Future
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.Size
import java.math.BigDecimal
import java.time.Instant
import java.util.*

@Entity
@Table(name = "ALL_PRIMITIVES_DATA")
@Schema(name = "AllPrimitivesData", description = "All possible primitive data types ")
data class AllPrimitivesData(

    @Id
    @Column(name = "uuid", nullable = false, updatable = false, unique = true, columnDefinition = "uuid")
    val uuid: UUID = UUID.randomUUID(),

    val blobData: ByteArray?,
    val booleanData: Boolean?,
    @Min(value = 18, message = "integerData should not be less than 18")
    @Max(value = 150, message = "integerData should not be greater than 150")
    val integerData: Int?,
    val textData: String?,
    @Future
    val dateData: Instant?,
    val decimalData: BigDecimal?,
    val floatData: Double?,
    val longData: Long?,
    val dateTimeData: Instant?,
    val timeData: Instant?,
    @Size(min = 0, max = 255, message = "stringData must be between 0 and 255 characters")
    val stringData: String?,

    @Email(message = "Email should be valid")
    val emailData: String?,

    val enumData: StringDataEnum
)
