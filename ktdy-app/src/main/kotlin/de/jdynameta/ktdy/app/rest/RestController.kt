package de.jdynameta.ktdy.app.rest

import de.jdynameta.ktdy.app.config.OpenApiConfig
import de.jdynameta.ktdy.app.entity.*
import de.jdynameta.ktdy.base.dto.TypedReflectionValueObjectWrapper
import de.jdynameta.ktdy.jpa.JpaMetaModelReader
import de.jdynameta.ktdy.jpa.reader.JpaReader
import de.jdynameta.ktdy.json.JsonWriter
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.io.StringWriter

@RestController
class RestController(
        @field:Autowired private val addressRepo: AddressEntityRepository,
        @field:Autowired private val primitiveDataRepo: AllPrimitivesDataRepository,
        @field:Autowired private val rootObjectRepo: RootObjectRepository,
        @field:PersistenceContext private val entityManager: EntityManager
) {

    @Operation( summary = "Get all addresses",  description = "Get all addresses from db" )
    @ApiResponses(
            value = [
                ApiResponse(responseCode = "200", description = "All Addresses"),
                ApiResponse(responseCode = "400", description = "On malformed request", content = [Content()])
            ]
    )
    @GetMapping("/allAddresses")
    fun allAddresses(): List<AddressEntity> {
        return this.addressRepo.findAll().asSequence().toList()
    }

    @Operation( summary = "Get all primitivData objects", description = "Get all primitivData objects  from db")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "All primitivData"),
            ApiResponse(responseCode = "400", description = "On malformed request", content = [Content()])
        ]
    )
    @GetMapping("/allPrimitiveData")
    fun allPrimitiveData(): List<AllPrimitivesData> {
        return this.primitiveDataRepo.findAll().asSequence().toList()
    }

    @Operation( summary = "Get all root objects", description = "Get all root objects  from db")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "All root objects "),
            ApiResponse(responseCode = "400", description = "On malformed request", content = [Content()])
        ]
    )
    @GetMapping("/allRootObjects")
    fun allRootObjects(): List<RootObject> {
        return this.rootObjectRepo.findAll().asSequence().toList()
    }

    @GetMapping(OpenApiConfig.GENERIC_PATH + "{className}", produces=["application/json"])
    fun allObject(@PathVariable className: String): ResponseEntity<String> {

        val metaRepo = JpaMetaModelReader().createJpaClassInfos(entityManager.metamodel, "KtDyTestApp")
        val infoToRead = metaRepo.infoWithName(className)
        if (infoToRead != null) {
            val readObjects = JpaReader().readAllObjectsFromDbClassInfo(infoToRead, this.entityManager)
            val valueObjects = readObjects.map {
                TypedReflectionValueObjectWrapper(it, infoToRead)
            }
            val writerOut = StringWriter()
            JsonWriter().writeObjectList(
                out = writerOut,
                objectList = valueObjects,
                persistenceType = JsonWriter.Operation.READ
            )
            return ResponseEntity<String>(writerOut.toString(), HttpStatus.OK)
        } else {
            return ResponseEntity<String>("{ \"error:\" Unknown class name : $className}", HttpStatus.NOT_FOUND)
        }
    }

    @Operation(
            summary = "Get TestDto",
            description = "Get example TestDto"
    )
    @ApiResponses(
            value = [
                ApiResponse(responseCode = "200", description = "All Addresses"),
                ApiResponse(responseCode = "400", description = "On malformed request", content = [Content()])
            ]
    )
    @GetMapping("/allTestdata")
    fun allTestdata(): TestDto {
        return TestDto(bpna = "fsdfsf")
    }
}