package de.jdynameta.ktdy.app.entity

import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface AllPrimitivesDataRepository: CrudRepository<AllPrimitivesData, UUID>