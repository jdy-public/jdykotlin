
package de.jdynameta.ktdy.app.entity

import com.fasterxml.jackson.annotation.JsonUnwrapped
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size
import java.util.Locale.IsoCountryCode

@Entity
@Table(name = "sync_records")
@Schema(name = "AddressEntity", description = "Address schema name")
class AddressEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "swag_sequence")
    @SequenceGenerator(name = "swag_sequence", sequenceName = "swag_sequence", allocationSize = 1)
    @Column(name = "id", nullable = false, updatable = false, insertable = false)
    val id: Long = 0,

    @Column(name = "country")
    @Enumerated(EnumType.STRING)
    val country: IsoCountryCode,

    @Column(name = "postcode")
    @get:Schema(description = "postCode")
    @NotBlank
    @Size(min = 0, max = 5)
    val postCode: String? = null,

    @Column
    val name: String? = null,

    @Column
    @NotBlank
    @Size(min = 0, max = 4)
    val houseNumber: String? = null,

) {
    constructor() : this(country = IsoCountryCode.PART3 ) {

    }
}
