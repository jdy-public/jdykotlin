package de.jdynameta.ktdy.app

import de.jdynameta.ktdy.app.entity.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationListener
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import java.math.BigDecimal
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

@SpringBootApplication
@EntityScan(basePackages=["de.jdynameta.ktdy.app"])
@EnableJpaRepositories(basePackages = ["de.jdynameta.ktdy.app"])
class AnalyseswagApplication(

    @field:Autowired private val addressRepo: AddressEntityRepository,
    @field:Autowired private val rootObjectRepo: RootObjectRepository,
    @field:Autowired private val primitiveDataRepo: AllPrimitivesDataRepository,
) : ApplicationListener<ApplicationReadyEvent> {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        createTestData()
    }

    private fun createTestData() {

        val place = AddressEntity(
            country = Locale.IsoCountryCode.PART3,
            houseNumber = "8",
            name = "My Address",
            postCode = "78446"
        )
        this.addressRepo.save(place)

        val id = 123L
        val allPrimitivesData = AllPrimitivesData(
            booleanData = true,
            blobData = null,
            integerData = id.toInt(),
            textData = "Text$id",
            dateData = Instant.now(),
            decimalData = BigDecimal("66"+id),
            floatData = (id * 1.7).toDouble(),
            longData = id * 3,
            dateTimeData = Instant.now().plus(id, ChronoUnit.DAYS),
            timeData = Instant.now().plus(id, ChronoUnit.HOURS),
            stringData = "String$id",
            emailData = "$id@example.com",
            enumData = StringDataEnum.ENUM_VALUE_3
        )
        this.primitiveDataRepo.save(allPrimitivesData)

        val dependent1 = DependentData(nameDependent= "dependetName1", dependentData = "data1")
        val dependent2 = DependentData(nameDependent= "dependetName2", dependentData = "data2")

        val rootObject = RootObject(
            nameRoot = "Root1",
            primitivData = allPrimitivesData,
            dependentObjects = listOf(dependent1, dependent2),

        )
        this.rootObjectRepo.save(rootObject)

    }
}

fun main(args: Array<String>) {
    runApplication<AnalyseswagApplication>(*args)
}
