package de.jdynameta.ktdy.app.rest

import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "TestDto", description = "TestDto description")
data class TestDto(
        @get:Schema(description = "Business test data")
        val bpna: String,
        )
