package de.jdynameta.ktdy.app.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.util.*

@Entity
@Table(name = "DEPENDENT_DATA")
data class DependentData (

    @Id
    @Column(name = "uuid", nullable = false, updatable = false, unique = true, columnDefinition = "uuid")
    val uuid: UUID = UUID.randomUUID(),

    val nameDependent: String,
    val dependentData: String,
)
