/*******************************************************************************
 * Copyright (c) 2021,2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package de.jdynameta.ktdy.app.config

import de.jdynameta.ktdy.jpa.JpaMetaModelReader
import de.jdynameta.ktdy.json.SwaggerCreator
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.PathItem
import io.swagger.v3.oas.models.info.Info
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext
import org.springdoc.core.customizers.OpenApiCustomizer
import org.springdoc.core.models.GroupedOpenApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class OpenApiConfig() {

    companion object {
        const val GENERIC_PATH = "/generic/"
    }

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    @Bean
    fun customOpenAPI(): OpenAPI? {

        val metaRepo = JpaMetaModelReader().createJpaClassInfos(entityManager.metamodel, "KtDyTestApp")


        val result = OpenAPI()
            .info(
                Info()
                    .title("Generated Title")
                    .description("Generated description")
                    .version("Generated version")
            )

        val swaggerCreator = SwaggerCreator()
        metaRepo.allClassInfos.forEach {
            result.schema(swaggerCreator.entityName(it), swaggerCreator.createSchema(it, false))

            val generatedPath = PathItem()
            generatedPath.operation(PathItem.HttpMethod.GET, swaggerCreator.createGetPath(it, "Generated"))
            generatedPath.operation(PathItem.HttpMethod.POST, swaggerCreator.createPostPath(it, "Generated"))
            result.path(GENERIC_PATH + swaggerCreator.entityName(it), generatedPath)
        }

        return result
    }

    @Bean
    fun openApiDefinition(): GroupedOpenApi {
        return GroupedOpenApi.builder()
            .group("docs")
            .pathsToMatch("/**")
            .displayName("Docs")
            .build()
    }

    fun sortSchemaCustomiser(): OpenApiCustomizer {
        return OpenApiCustomizer { openApi: OpenAPI ->
            openApi.components(with(openApi.components) {
                schemas(schemas?.values?.sortedBy { it.name }?.associateBy { it.name })
            })
        }
    }

}
