package de.jdynameta.ktdy.app.entity

import org.springframework.data.repository.CrudRepository
import java.util.*

interface RootObjectRepository: CrudRepository<RootObject, UUID> {
}